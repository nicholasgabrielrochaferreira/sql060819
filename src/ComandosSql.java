
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;

public class ComandosSql {

    public static Connection obterConexao() {
        try {
            Class.forName("org.postgresql.Driver");
            String banco = "jdbc:postgresql://10.90.24.54/nicholas";
            String usuario = "nicholas";
            String senha = "mtab80";
            return DriverManager.getConnection(banco, usuario, senha);
        } catch (ClassNotFoundException | SQLException ex) {
            return null;
        }
    }

    public void listar(JList lista) {
        try {
            lista.removeAll();
            DefaultListModel dlm = new DefaultListModel();
            Connection c = obterConexao();
            String SQL = "select * from sc_tabela_periodica.elemento_quimico";
            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);
            while (rs.next()) {
                dlm.addElement(rs.getString("numero_atomico"));
            }
            lista.setModel(dlm);
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ComandosSql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void adicionarSubstanciaSimples(String nomesubstancia, String numeroatomicoe, String quantidadee) {
        try {
            Connection c = obterConexao();
            String SQL = "insert into sc_tabela_periodica.substanciasimples(nomesubstancia, numeroatomicoe, quantidadee) values(?, ?, ?)";
            PreparedStatement ps = c.prepareStatement(SQL);
            
            ps.setString(1, nomesubstancia);
            ps.setString(2, numeroatomicoe);
            ps.setString(3, quantidadee);
            
            ps.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(ComandosSql.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
